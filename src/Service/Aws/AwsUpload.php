<?php

namespace Moofik\Service\Aws;

class AwsUpload
{
    private const DEFAULT_ACL = 'TEST_ROLE';

    /**
     * @param $post_id
     * @param null $data
     * @param null $file_path
     * @param bool $force_new_s3_client
     * @param bool $remove_local_files
     *
     * @return array|null
     * @throws AwsUploadException
     */
    function upload_file_to_s3($post_id, $data = null, $file_path = null, $force_new_s3_client = false, $remove_local_files = true)
    {
        $return_metadata = $data;

        if (is_null($data)) {
            $data = wp_get_attachment_metadata($post_id, true);
        }

        if (is_wp_error($data)) {
            throw new AwsUploadException('Error in AWS attachment data');
        }

        $this->check_if_need_cancel($post_id, $data);

        if (is_null($file_path)) {
            $file_path = get_attached_file($post_id, true);
        }

        $file_name = $this->get_file_name($file_path);
        $type = get_post_mime_type($post_id);

        $this->check_if_mime_type_allowed($type);

        $old_s3object = $this->get_attachment_s3_info($post_id);
        $acl = $this->get_acl($post_id, $old_s3object, $data);
        $s3object = $this->get_s3object($post_id, $data, $acl, $file_path, $file_name, $old_s3object);
        $s3client = $this->get_s3client($s3object['region'], $force_new_s3_client);
        $args = $this->get_args($s3object, $acl, $type, $post_id);

        $s3client->putObject($args);

        $metadata = $this->remove_files($post_id, $s3object, $data, $remove_local_files);
        $this->resolve_attachment_metadata($post_id, $metadata, $return_metadata);

        if (!is_null($return_metadata)) {
            return $data;
        }

        return $s3object;
    }

    /**
     * Получает массив с данными необходимыми для проведения загрузки в S3
     *
     * @param int $post_id
     * @param array $data
     * @param string $acl
     * @param string $file_path
     * @param string $file_name
     * @param array|null $old_s3object
     *
     * @return array
     */
    private function get_s3object(
        int $post_id,
        array $data,
        string $acl,
        string $file_path,
        string $file_name,
        array $old_s3object = null
    ): array {
        if ($old_s3object) {
            $prefix = dirname($old_s3object['key']);
            $prefix = ('.' === $prefix) ? '' : $prefix . '/';
            $bucket = $old_s3object['bucket'];
            $region = '';

            if (isset($old_s3object['region'])) {
                $region = $old_s3object['region'];
            };
        } else {
            if (isset($data['file'])) {
                $time = $this->get_folder_time_from_url($data['file']);
            } else {
                $time = $this->get_attachment_folder_time($post_id);
                $time = date('Y/m', $time);
            }

            $prefix = $this->get_file_prefix($time);
            $bucket = $this->get_setting('bucket');
            $region = $this->get_setting('region');

            if (is_wp_error($region)) {
                $region = '';
            }
        }

        $s3object = [
            'bucket' => $bucket,
            'key' => $prefix . $file_name,
            'region' => $region,
            'file_path' => $file_path
        ];

        if ($acl != self::DEFAULT_ACL) {
            $s3object['acl'] = $acl;
        }

        return $s3object;
    }

    /**
     * Возвращает права доступа для работы с AWS S3
     *
     * @param int $post_id
     * @param array $s3object
     * @param array $data
     *
     * @return string
     */
    private function get_acl(int $post_id, array $s3object, array $data): string
    {
        $acl = self::DEFAULT_ACL;

        if (is_array($s3object) && isset($s3object['acl'])) {
            $acl = $s3object['acl'];
        }

        return apply_filters('as3cf_upload_acl', $acl, $data, $post_id);
    }

    /**
     * Сохраняет/очищает метаданные файла для дальнейшего использования в WP, если это необходимо
     *
     * @param int $post_id
     * @param array $data
     * @param array $return_metadata
     */
    private function resolve_attachment_metadata(int $post_id, array $data, array $return_metadata = null)
    {
        $need_remove_local_files = $this->get_setting('remove-local-file');
        $filesize_total = $data['filesize_total'];
        unset($data['filesize_total']);

        if ($need_remove_local_files && $filesize_total > 0) {
            update_post_meta($post_id, 'wpos3_filesize_total', $filesize_total);

            return;
        }

        if (isset($data['filesize'])) {
            unset($data['filesize']);

            if (is_null($return_metadata)) {
                update_post_meta($post_id, '_wp_attachment_metadata', $data);
            }

            delete_post_meta($post_id, 'wpos3_filesize_total');
        }
    }

    /**
     * Получает массив аргументов для отправки файла через клиент s3
     *
     * @param array $s3object
     * @param string $acl
     * @param $type
     * @param $post_id
     *
     * @return array
     */
    private function get_args(array $s3object, string $acl, $type, $post_id): array
    {
        $args = [
            'Bucket' => $s3object['bucket'],
            'Key' => $s3object['key'],
            'SourceFile' => $s3object['file_path'],
            'ACL' => $acl,
            'ContentType' => $type,
            'CacheControl' => 'max-age=31536000',
            'Expires' => date('D, d M Y H:i:s O', time() + 31536000),
        ];

        $args = apply_filters('as3cf_object_meta', $args, $post_id);

        return $args;
    }

    /**
     * Удаляет файлы после загрузки в AWS S3
     *
     * @param int $post_id
     * @param array $s3object
     * @param $data
     * @param $remove_local_files
     *
     * @return array массив с метаданными об удаленных файлах
     */
    private function remove_files(int $post_id, array $s3object, array $data, $remove_local_files): array
    {
        $file_path = $s3object['file_path'];
        $files_to_remove = [];
        $metadata = [];

        if (file_exists($file_path)) {
            $files_to_remove[] = $file_path;
        }

        delete_post_meta($post_id, 'amazonS3_info');
        add_post_meta($post_id, 'amazonS3_info', $s3object);

        $file_paths = $this->get_attachment_file_paths($post_id, true, $data);
        $additional_images = [];
        $filesize_total = 0;
        $remove_local_files_setting = $this->get_setting('remove-local-file');

        if ($remove_local_files_setting) {
            $bytes = filesize($file_path);

            if (false !== $bytes) {
                $metadata['filesize'] = $bytes;
                $filesize_total += $bytes;
            }
        }

        foreach ($file_paths as $file_path) {
            if (!in_array($file_path, $files_to_remove)) {
                $additional_images[] = [
                    'Key' => $s3object['prefix'] . basename($file_path),
                    'SourceFile' => $file_path,
                ];

                $files_to_remove[] = $file_path;

                if ($remove_local_files_setting) {
                    $bytes = filesize($file_path);

                    if (false !== $bytes) {
                        $filesize_total += $bytes;
                    }
                }
            }
        }

        if ($remove_local_files) {
            if ($remove_local_files_setting) {
                $files_to_remove = apply_filters('as3cf_upload_attachment_local_files_to_remove', $files_to_remove, $post_id, $file_path);

                $files_to_remove = array_unique($files_to_remove);

                $this->remove_local_files($files_to_remove);
            }
        }

        $metadata['filesize_total'] = $filesize_total;

        return $metadata;
    }

    /**
     * @param int $post_id
     * @param array $data
     *
     * @throws AwsUploadException
     */
    private function check_if_need_cancel(int $post_id, array $data)
    {
        $pre = apply_filters('as3cf_pre_upload_attachment', false, $post_id, $data);
        if (false !== $pre) {
            $error_msg = is_string($pre) ? $pre : __('Upload aborted by filter \'as3cf_pre_upload_attachment\'', 'amazon-s3-and-cloudfront');

            throw new AwsUploadException($error_msg);
        }
    }

    /**
     * @param string $type
     *
     * @throws AwsUploadException
     */
    private function check_if_mime_type_allowed($type)
    {
        $allowed_types = $this->get_allowed_mime_types();

        if (!in_array($type, $allowed_types)) {
            $error_msg = sprintf(__('Mime type %s is not allowed', 'amazon-s3-and-cloudfront'), $type);

            throw new AwsUploadException($error_msg);
        }
    }

    /**
     * @param int $post_id
     * @param $file_path
     *
     * @return string
     * @throws AwsUploadException
     */
    private function get_file_name($file_path): string
    {
        if (!file_exists($file_path)) {
            $error_msg = sprintf(__('File %s does not exist', 'amazon-s3-and-cloudfront'), $file_path);

            throw new AwsUploadException($error_msg);
        }

        return basename($file_path);
    }

    /**
     * Dummy method (заглушка)
     */
    private function remove_local_files(array $file_paths, $attachment_id = 0)
    {
    }

    /**
     * Dummy method (заглушка)
     *
     * @return array
     */
    private function get_allowed_mime_types(): array
    {
        return ['image/jpeg'];
    }

    /**
     * Dummy method (заглушка)
     *
     * @param int $post_id
     *
     * @return int
     */
    private function get_attachment_folder_time(int $post_id): int
    {
        return 15000000;
    }

    /**
     * Dummy method (заглушка)
     *
     * @param string $file
     *
     * @return string
     */
    private function get_file_prefix(string $file): string
    {
        return 'dummy_';
    }

    /**
     * Dummy method (заглушка)
     *
     * @param string $string
     *
     * @return string
     */
    private function get_setting(string $string): string
    {
        return 'dummy';
    }

    /**
     * Dummy method (заглушка)
     *
     * @param int $post_id
     * @param bool $param
     * @param array $data
     *
     * @return array
     */
    private function get_attachment_file_paths(int $post_id, bool $param, array $data): array
    {
        return [];
    }

    /**
     * Dummy method (заглушка)
     *
     * @param int $id
     *
     * @return array
     */
    private function get_attachment_s3_info(int $id): array
    {
        return [];
    }

    /**
     * Dummy method (заглушка)
     *
     * @param string $url
     *
     * @return string
     */
    private function get_folder_time_from_url(string $url): string
    {
        return '';
    }

    /**
     * Dummy method (заглушка)
     *
     * @param string $region
     * @param $force_new_s3_client
     *
     * @return DummyS3Client
     */
    private function get_s3client(string $region, $force_new_s3_client): DummyS3Client
    {
        return new DummyS3Client();
    }
}

/**
 * Global dummy methods! (глобальные заглушки для методов WordPress)
 */

if (!function_exists('is_wp_error')) {
    function is_wp_error($errorObject)
    {
        return false;
    }
}

if (!function_exists('apply_filters')) {
    function apply_filters(string $tag, $value, ...$vars)
    {
        return $value;
    }
}

if (!function_exists('update_post_meta')) {
    function update_post_meta(int $post_id, string $meta_key, $meta_value, $prev_value = null)
    {
        return true;
    }
}

if (!function_exists('delete_post_meta')) {
    function delete_post_meta(int $post_id, string $meta_key, $meta_value = null)
    {
        return true;
    }
}

if (!function_exists('wp_get_attachment_metadata')) {
    function wp_get_attachment_metadata(int $post_id, bool $unfiltered)
    {
        return [];
    }
}

if (!function_exists('add_post_meta')) {
    function add_post_meta(int $post_id, string $meta_key, $meta_value, bool $unique = false)
    {
        return true;
    }
}

if (!function_exists('get_post_mime_type')) {
    function get_post_mime_type(int $post_id)
    {
        return 'image/jpeg';
    }
}

if (!function_exists('get_attached_file')) {
    function get_attached_file(int $attachment_id, bool $unfiltered = false)
    {
        return 'test.jpg';
    }
}

if (!function_exists('__')) {
    function __(string $text, string $domain)
    {
        return $text;
    }
}