<?php

namespace Moofik\Service\Obfuscator;

class Obfuscator
{
    /**
     * @var Faker
     */
    private $faker;

    /**
     * @var array
     */
    private $replacements;

    /**
     * Obfuscator constructor.
     *
     * @param Faker $faker
     * @param array $replacements Массив конфигурирующий обфусцирование полей
     */
    public function __construct(Faker $faker, array $replacements = [])
    {
        $this->faker = $faker;
        $this->replacements = $replacements;
    }

    /**
     * Обфусцирует переданный в виде строки дамп базы данных
     *
     * @param string $content
     *
     * @return string
     */
    public function obfuscate(string $content): string
    {
        foreach ($this->replacements as $table => $replacements) {
            $insertQueryForTable = $this->getInsertQueryForTable($table, $content);
            $insertQueryParts = $this->getInsertQueryParts($insertQueryForTable[0]);
            $insertIntoPart = array_shift($insertQueryParts);
            $columnIndexMapping = $this->getInsertIntoIndexMapping($insertIntoPart);

            foreach ($insertQueryParts as $valuePart) {
                $valuesIndexMapping = $this->getValuesIndexMapping($valuePart);

                foreach ($replacements as $replacementKey => $replacementValue) {
                    $index = array_search($replacementKey, $columnIndexMapping);
                    $valueToReplace = $valuesIndexMapping[$index];

                    if (is_array($replacementValue)) {
                        $type = current($replacementValue);
                        $replacementValue = $this->faker->get($type);
                    }

                    $content = str_replace($valueToReplace, $replacementValue, $content);
                }
            }
        }

        return $content;
    }

    /**
     * @param string $table
     * @param string $content
     *
     * @return array
     */
    private function getInsertQueryForTable(string $table, string $content): array
    {
        $regex = '/(INSERT INTO `' . $table . '`.*?\;)/ius';

        preg_match_all($regex, $content, $matches);

        return $matches[0];
    }

    /**
     * @param string $query
     *
     * @return array
     */
    private function getInsertQueryParts(string $query): array
    {
        $regex = '/\(.*?\)/iu';

        preg_match_all($regex, $query, $matches);

        return $matches[0];
    }

    /**
     * @param string $insertIntoPart
     *
     * @return array
     */
    private function getInsertIntoIndexMapping(string $insertIntoPart): array
    {
        $regex = '/`(.*?)`/iu';

        preg_match_all($regex, $insertIntoPart, $matches);

        return $matches[1];
    }

    /**
     * @param string $valuesPart
     *
     * @return array
     */
    private function getValuesIndexMapping(string $valuesPart): array
    {
        $regex = '/\'(.*?)\'|([a-z0-9]+)/iu';

        preg_match_all($regex, $valuesPart, $matches);

        return $matches[1];
    }
}