<?php

namespace Moofik\Service\Obfuscator;

class Faker
{
    /**
     * @var array
     */
    private $dummyMap = [
        'email' => 'test@test.ru',
        'phone' => '+79053334455'
    ];

    /**
     * @param $key
     *
     * @return string
     */
    public function get($key): string
    {
        return isset($this->dummyMap[$key]) ? $this->dummyMap[$key] : '';
    }
}