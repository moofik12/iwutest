<?php

namespace Moofik\Service\SyntaxChecker;

class SyntaxChecker
{
    private const BRACES_PAIRS = [
        '(' => ')',
        '{' => '}',
        '[' => ']',
        '<' => '>'
    ];

    /**
     * @var array
     */
    private $characterStack;

    /**
     * @param string $sequence Последовательность символов которую нужно проверить на синтексическую верность последовательности скобок
     *
     * @return int Возвращает 1 в случае ошибки, в противном случае 0
     */
    public function checkBraces(string $sequence): int
    {
        $this->characterStack = [];
        $chars = str_split($sequence);

        foreach ($chars as $char) {
            if (key_exists($char, self::BRACES_PAIRS)) {
                array_push($this->characterStack, $char);

                continue;
            }

            if (in_array($char, self::BRACES_PAIRS)) {
                if (empty($this->characterStack)) {
                    return 1;
                }

                $lastOpeningBracket = array_pop($this->characterStack);
                $matchingClosingBracket = self::BRACES_PAIRS[$lastOpeningBracket];

                if ($matchingClosingBracket !== $char) {
                    return 1;
                }
            }
        }

        return empty($this->characterStack) ? 0 : 1;
    }
}