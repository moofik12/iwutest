<?php

namespace Moofik\Service\Storage;

class Storage
{
    /**
     * @param string $name
     * @param string $content
     *
     * @return bool|int
     */
    public function createFile(string $name, string $content)
    {
        return file_put_contents($name, $content);
    }

    /**
     * @param $path
     *
     * @return bool|string
     */
    public function readFile($path)
    {
        return file_get_contents($path);
    }
}