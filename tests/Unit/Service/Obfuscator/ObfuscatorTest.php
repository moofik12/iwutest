<?php

namespace Moofik\Test\Unit\Service\Obfuscator;

use Moofik\Service\Obfuscator\Faker;
use Moofik\Service\Obfuscator\Obfuscator;
use PHPUnit\Framework\TestCase;

class ObfuscatorTest extends TestCase
{
    public function testObfuscate()
    {
        $dump = <<<SQL
INSERT INTO `test` (`field1`, `field2`)
VALUES ('test1', 'test2');
SQL;
        $expectedDump = <<<SQL
INSERT INTO `test` (`field1`, `field2`)
VALUES ('test@yandex.ru', 'defined');
SQL;
        $config = [
            'test' => [
                'field1' => ['type' => 'email'],
                'field2' => 'defined'
            ]
        ];

        $faker = $this->createMock(Faker::class);
        $faker
            ->method('get')
            ->willReturn('test@yandex.ru');

        $obfuscator = new Obfuscator($faker, $config);
        $actualDump = $obfuscator->obfuscate($dump);

        $this->assertEquals($expectedDump, $actualDump);
    }
}