<?php

namespace Moofik\Test\Unit\Service\SyntaxChecker;

use Moofik\Service\SyntaxChecker\SyntaxChecker;
use PHPUnit\Framework\TestCase;

class SyntaxCheckerTest extends TestCase
{
    /**
     * @var SyntaxChecker
     */
    private $syntaxChecker;

    protected function setUp()
    {
        $this->syntaxChecker = new SyntaxChecker();
    }

    /**
     * @var string $value
     *
     * @dataProvider successValuesProvider
     */
    public function testCheckBracesIsSuccessful(string $value)
    {
        $result = $this->syntaxChecker->checkBraces($value);
        $this->assertEquals(0, $result);
    }

    /**
     * @var string $value
     *
     * @dataProvider errorValuesProvider
     */
    public function testCheckBracesReturnError(string $value)
    {
        $result = $this->syntaxChecker->checkBraces($value);
        $this->assertEquals(1, $result);
    }

    /**
     * @return array
     */
    public function successValuesProvider()
    {
        return [
            ["---(++++)----"],
            [""],
            ["before ( middle []) after "],
            ["(  [  <>  ()  ]  <>  )"],
        ];
    }

    /**
     * @return array
     */
    public function errorValuesProvider()
    {
        return [
            [") ("],
            ["} {"],
            ["<(   >)"],
            ["   (      [)"],
        ];
    }
}