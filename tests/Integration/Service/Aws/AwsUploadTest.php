<?php

namespace Moofik\Test\Integration\Service\Aws;

use Moofik\Service\Aws\AwsUpload;
use PHPUnit\Framework\TestCase;

class AwsUploadTest extends TestCase
{
    public function testUploadFileToS3()
    {
        $awsUpload = new AwsUpload();

        touch('test.jpg');

        $result = $awsUpload->upload_file_to_s3(1);

        unlink('test.jpg');

        $expected = [
            'bucket' => 'dummy',
            'key' => 'dummy_test.jpg',
            'region' => 'dummy',
            'file_path' => 'test.jpg'
        ];

        $this->assertEquals($expected, $result);
    }
}