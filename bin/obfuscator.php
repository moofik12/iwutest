<?php

use Moofik\Service\Storage\Storage;
use Moofik\Service\Obfuscator\Obfuscator;
use Moofik\Service\Obfuscator\Faker;
use Symfony\Component\Yaml\Yaml;

require __DIR__.'/../vendor/autoload.php';

$storage = new Storage();

$sqlDump = $storage->readFile(__DIR__.'/../var/example.dump.sql');

$faker = new Faker();
$obfuscatorConfig = Yaml::parseFile(__DIR__.'/../config/obfuscator.yml');
$obfuscator = new Obfuscator($faker, $obfuscatorConfig);

$newDumpContent = $obfuscator->obfuscate($sqlDump);

$storage->createFile(__DIR__.'/../var/new.dump.sql', $newDumpContent);